const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

app.get('/', function(req,res){
  res.send('Olá mundo do Docker e Cordova!!!')
});

app.get('/apk/:project/:platform', function(req,res){
  const {project,platform} = req.params
  let {version} = req.query

  if(!version){
    version = 'apk-'+new Date().toISOString().
    replace(/T/, ' '). 
    replace(/\..+/, '') + '.apk'
  }else{
    version += '.apk'
  }
  const file =  __dirname+`/${project}/platforms/${platform}/build/outputs/apk/android-debug.apk`
  if(fs.existsSync(file)){
    res.download(file,version );
  }else{
    res.status(500).send({
      error:'Error',
      project,
      file
    })
  }
  
});




const NODE_PORT = process.env.NODE_PORT || 9000
app.listen(NODE_PORT,'0.0.0.0', () => console.log(`Server ativo na porta ${NODE_PORT}`));